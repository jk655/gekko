package com.citi.training.rest.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.data.bs.StockService;
import com.citi.training.data.bs.StrategyService;
import com.citi.training.data.bs.TradeService;
import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Strategy;
import com.citi.training.data.dto.Trade;

@RestController
@RequestMapping("/strategies")
public class StrategyRestController {

	public static final Logger log = LoggerFactory.getLogger(StrategyRestController.class);

	@Autowired
	private StrategyService strategyService;

	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" })
	public List<Strategy> getAll() {
		return strategyService.getAllStrategies();
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	public Strategy createStrategy(@RequestBody Strategy strategy) {
		log.debug("REST Controller createStock:\n" + strategy);
		return strategyService.saveStrategy(strategy);
	}

	@RequestMapping(method = RequestMethod.GET, value = "stockid/{id:[0-9]*}", produces = { "application/json" })
	public Strategy getByStockId(@PathVariable int id) {
		log.debug("REST Stock Controller getById: " + id);
		return strategyService.getStrategyByStockId(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "strategyid/{id:[0-9]*}", produces = { "application/json" })
	public Strategy getByStrategyId(@PathVariable int id) {
		log.debug("REST Controller getById: " + id);
		return strategyService.getStrategyByStrategyId(id);
	}

}