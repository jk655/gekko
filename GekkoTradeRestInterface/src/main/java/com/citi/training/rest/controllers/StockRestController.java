package com.citi.training.rest.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.data.bs.StockService;
import com.citi.training.data.bs.TradeService;
import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Trade;

@RestController
@RequestMapping("/stocks")
public class StockRestController {

	public static final Logger log = LoggerFactory.getLogger(StockRestController.class);

	@Autowired
	private StockService stockService;

	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" })
	public List<Stock> getAll() {
		return stockService.getAllStocks();
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	public Stock createStock(@RequestBody Stock newStock) {
		log.debug("REST Controller createStock:\n" + newStock);
		return stockService.saveStock(newStock);
	}

	@RequestMapping(method = RequestMethod.GET, value = "id/{id:[0-9]*}", produces = { "application/json" })
	public Stock getById(@PathVariable int id) {
		log.debug("REST Stock Controller getById: " + id);
		return stockService.getStockById(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "ticker/{ticker:^[a-zA-Z]*}", produces = { "application/json" })
	public List<Stock> getByTicker(@PathVariable String ticker) {
		log.debug("REST Controller getById: " + ticker);
		return stockService.getStocksByTicker(ticker);
	}

}