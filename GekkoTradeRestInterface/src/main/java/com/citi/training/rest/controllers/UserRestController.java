package com.citi.training.rest.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.data.bs.StockService;
import com.citi.training.data.bs.StrategyService;
import com.citi.training.data.bs.TradeService;
import com.citi.training.data.bs.UserService;
import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Strategy;
import com.citi.training.data.dto.Trade;
import com.citi.training.data.dto.User;

@RestController
@RequestMapping("/users")
public class UserRestController {
	public static final Logger log = LoggerFactory.getLogger(UserRestController.class);

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" })
	public List<User> getAll() {
		return userService.getAllUsers();
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	public User createUser(@RequestBody User user) {
		return userService.saveUser(user);
	}

	@RequestMapping(method = RequestMethod.GET, value = "userid/{id:[0-9]*}", produces = { "application/json" })
	public User getByUserId(@PathVariable int id) {
		return userService.getUserById(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/buyTotal", produces = { "application/json" })
	public double getAmountBuy() {
		return userService.getAmountBuy();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/sellTotal", produces = { "application/json" })
	public double getAmountSell() {
		return userService.getAmountSell();
	}
	


}