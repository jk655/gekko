/**
 * 
 */
$(document).ready(startup);

function startup(){
	
	lineChart();
	setInterval(prices,2000);
	setInterval(lineChart,2000);
	
}


function getGoogData(){
	var arrLastFive;
	$.ajax({
		url: '/stocks/ticker/GOOG',
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			arrLastFive = response.slice(-7);	
		}
	});
	return arrLastFive;
}

function getMsftData(){
	var arrLastFive;
	$.ajax({
		url: '/stocks/ticker/MSFT',
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			arrLastFive = response.slice(-7);	
		}
	});
	return arrLastFive;
}
function getAaplData(){
	var arrLastFive;
	$.ajax({
		url: '/stocks/ticker/AAPL',
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			arrLastFive = response.slice(-7);	
		}
	});
	return arrLastFive;
}


function lineChart(){
	
	var gData = getGoogData();
	var mData = getMsftData();
	var aData = getAaplData();
	    
	google.charts.load('current', {'packages':['corechart']});

	google.charts.setOnLoadCallback(drawBasic);

	function drawBasic() {	 
	      var data = new google.visualization.DataTable();
	      data.addColumn('number', 'X');
	      data.addColumn('number', 'GOOG');
	      data.addColumn('number', 'MSFT');
	      data.addColumn('number', 'AAPL');

	      for (var i = 0; i <6; i++) {
             // data.addRow([i, 100-(gData[i].price/gData[i+1].price * 100).toFixed(4), 100-(mData[i].price/mData[i+1].price * 100).toFixed(4),100-(aData[i].price/aData[i+1].price * 100).toFixed(4)]);
              data.addRow([i, (gData[i+1].price - gData[i].price), (mData[i+1].price - mData[i].price),(aData[i+1].price - aData[i].price)]);
          }      

	      var options = {
	        hAxis: {
	          title: 'Last 5 Change',
	        	  textStyle: {
		              color: '#c8c8c8',
		              // The transparency of the text.
		              opacity: 0.8
		            },
		            titleTextStyle: {
		                color: '#c8c8c8'
		            }
	        },
	        vAxis: {
	          title: 'Price',
	        	  textStyle: {
		              color: '#c8c8c8',
		              // The transparency of the text.
		              opacity: 0.8
		            },
		            titleTextStyle: {
		                color: '#c8c8c8'
		            }
	        },
	            
	        legend: {
	            textStyle: {
	                color: '#c8c8c8'
	            }
	        },
	        backgroundColor:'#3e444c'
	      };

	      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

	      chart.draw(data, options);
	    }
	
}

function prices(){
	//var tickerInput = "GOOG";
	//var url = "/stocks/ticker/"+ tickerInput;
	
	var urls = ['/stocks/ticker/GOOG','/stocks/ticker/MSFT', '/stocks/ticker/AAPL'];
	
	$.ajax({
		url: urls[0],
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			var output;
			
			var last = response.length - 1;				
			var lastArr = response[last];
			var previous = response.length - 2;
			var prevArr = response[previous];
			var change = prevArr.price / lastArr.price * 100;
			var percent = 100-change;
			var formatNum = percent.toFixed(4);
			output = lastArr.price;
			$("#googPrice").html(output);
			
			if(percent< 0)
			{
			$("#googPercent").html(formatNum+"<i class='fas fa-arrow-down fa-fw' align='right'></i>");
			$("#googPercent").css("background-color", "tomato");
			
			}
		else{
			$("#googPercent").html(formatNum+"<i class='fas fa-arrow-up fa-fw' align='right'></i>");
			$("#googPercent").css("background-color", "green");
		}
			
		}
	});
	$.ajax({
		url: urls[1],
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			var output;
			var last = response.length - 1;				
			var lastArr = response[last];
			var previous = response.length - 2;
			var prevArr = response[previous];
			var change = prevArr.price / lastArr.price * 100;
			var percent = 100-change;
			var formatNum = percent.toFixed(4);
			output = lastArr.price;
			$("#msftPrice").html(output);
			$("#msftPercent").html(formatNum);	
			if(percent< 0)
				{
				$("#msftPercent").html(formatNum+"<i class='fas fa-arrow-down fa-fw' align='right'></i>");
				$("#msftPercent").css("background-color", "tomato");
				}
			else{
				$("#msftPercent").html(formatNum+"<i class='fas fa-arrow-up fa-fw' align='right'></i>");
				$("#msftPercent").css("background-color", "green");
			}
		}
	});
	$.ajax({
		url: urls[2],
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			var output;
			var last = response.length - 1;				
			var lastArr = response[last];
			var previous = response.length - 2;
			var prevArr = response[previous];
			var change = prevArr.price / lastArr.price * 100;
			var percent = 100-change;
			var formatNum = percent.toFixed(4);
			output = lastArr.price;
			$("#aaplPrice").html(output);
			$("#aaplPercent").html(formatNum);	
			if(percent< 0)
			{
			$("#aaplPercent").html(formatNum+"<i class='fas fa-arrow-down fa-fw' align='right'></i>");
			$("#aaplPercent").css("background-color", "tomato");
			}
		else{
			$("#aaplPercent").html(formatNum+"<i class='fas fa-arrow-up fa-fw' align='right'></i>");
			$("#aaplPercent").css("background-color", "green");
		}
		}
	});
	
};