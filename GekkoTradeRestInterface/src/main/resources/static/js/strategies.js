/**
 * 
 */
$(document).ready(startup);

function startup(){
	dialogBoxes();
	strategyList();
	performanceList();
	getTradeData();
	setInterval(getTradeData,10000);
	setInterval(getProfitLoss,3000);
}

function dialogBoxes(){
	$( "#dialogStragStatus" ).dialog({
		modal: true,
		autoOpen: false,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
	});

	$( "#dialogStragAct" ).dialog({
		modal: true,
		autoOpen: false,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
	});

	$( "#dialogStragAdj" ).dialog({
		modal: true,
		autoOpen: false,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
	});

	$( "#dialogStragAdjSuccess" ).dialog({
		modal: true,
		autoOpen: false,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
	});
}

//for trade Feed
function getTradeData(){
	var arrLastFive;
	$.ajax({
		url: '/trades',
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			arrLastFive = response.slice(-5);
			var output= "<div id='tradeFeed' class='ticker'>";
			output +="<ul class='list-group'>";
			output += "<li class='list-group-item'>"+arrLastFive[0].price+" "+arrLastFive[0].stock+" "+arrLastFive[0].tradeType+" "+arrLastFive[0].state+"</li>";
			output += "<li class='list-group-item'>"+arrLastFive[1].price+" "+arrLastFive[1].stock+" "+arrLastFive[1].tradeType+" "+arrLastFive[1].state+"</li>";
			output += "<li class='list-group-item'>"+arrLastFive[2].price+" "+arrLastFive[2].stock+" "+arrLastFive[2].tradeType+" "+arrLastFive[2].state+"</li>";
			output += "<li class='list-group-item'>"+arrLastFive[3].price+" "+arrLastFive[3].stock+" "+arrLastFive[3].tradeType+" "+arrLastFive[3].state+"</li>";
			output += "<li class='list-group-item'>"+arrLastFive[4].price+" "+arrLastFive[4].stock+" "+arrLastFive[4].tradeType+" "+arrLastFive[4].state+"</li>";
			output += "</ul>";	
			output += "</div>";	
		$("#tradeShell").html(output);
		
		$.simpleTicker($("#tradeFeed"),{
			speed : 1000,
			delay : 2000,
			easing : 'swing',
			effectType : 'roll'
		});
		},
		error: function (err) {
			alert("Error: " + err.responseText)
		}
	});
}

//helper method for getting stock ticker
function getTicker(id){
	var tickerId = id;
	var ticker;
	$.ajax({
		url: '/stocks/id/'+tickerId,
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			ticker = response;
		}
	});
	return ticker;
}

//helper method to get trade object
function getTrade(id){
	var trade;
	$.ajax({
		url: '/trades/trade/'+ id,
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			trade = response;
			//alert(response.id);
		}
	});
	return trade;
}

//helper method to get strategy object
function getStrategy(id){
	var strategy;
	$.ajax({
		url: '/strategies/strategyid/'+ id,
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			strategy = response;
			//alert(response.id);
		}
	});
	return strategy;
}

//click handler setup for trade status
function statusSetup(length){
	var noButtons = length;
	for(var i=0; i<noButtons;i++){
		var id = $("#buttonStatus"+i).val();
		$("#buttonStatus"+i).click(getStatus);
	}
	
}

//get trade Status
function getStatus(){
	var id = $(this).val();
	//alert(id);
	var strag = getStrategy(id);
	//alert(strag.id);
	var stock = getTicker(strag.stock_id);
	$.ajax({
		url: "/trades/strategyid/"+ id,
		dataType: "json",
		type: "GET",
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			//alert(response[0].stock);
			var output= "<div>";
			output +="<ul class='list-group liststra'>";
			for(var i=0; i<response.length;i++){
				output += "<li class='list-group-item'>"+response[i].id+") "+response[i].stock+" "+response[i].tradeType +" "+ response[i].state+"</li>";
			}
			output += "</ul>";
			output += "</div>";
			
			$("#statusStrag").html(output);
			$( "#dialogStragStatus").dialog('open');
		}
	});	
}


function getProfitLoss(){
	var totalBuy;
	var totalSell;
	var total;
	$.ajax({
		url: "/users/buyTotal",
		dataType: "json",
		type: "GET",
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {	
			totalBuy = response;		
		}
	});
	
	$.ajax({
		url: "/users/sellTotal",
		dataType: "json",
		type: "GET",
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {	
			totalSell = response;
			
			total = totalBuy - totalSell;
			
			var output = "<h3>"+total+"</h3>";
			$("#profitLoss").html(output);		
		}
	});
	
}


//creates buttons to access strategy information and trades list
function performanceList(){
	
	$.ajax({
		url: "/strategies",
		dataType: "json",
		type: "GET",
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			var output="<ul class='list-group liststra'>";
			for(var i=0;i<response.length;i++)
				{
				var stock = getTicker(response[i].stock_id);
				var stringId = "buttonStatus"+i;
				output += "<li class='list-group-item'><button id='"+stringId+"' type='button' class='btn btn-default' value='"+response[i].strategy_id+"'>"+stock.ticker+" "+response[i].amount+"</button></li>";			
				}			
			output += "</ul>";		
			$("#listStrategy").html(output);
			statusSetup(response.length);
		},
		error: function (err) {
			alert("Error: " + err.responseText)
		}
	});
}

//click handler for activation button and adjust button for strategy
function actSetup(length){
	var noButtons = length;
	for(var i=0; i<noButtons;i++){
		$("#buttonAct"+i).click(updateState);
		$("#buttonAdj"+i).click(openStrategy);
	}
}

//requests strategy by id and opens dialog box for adjusting strategy
function openStrategy(){
	var id = $(this).val();
	
	$.ajax({
		url: '/strategies/strategyid/'+ id,
		type: "GET",
	    dataType: 'json',
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			strategy = response;
			if(response.strategy_status == 1){
				var output="<h3>Active</h3>";
			}
			else{
				var output="<h3>Inactive</h3>";
			}
			var stock = getTicker(response.stock_id);
			output +="<h4>Stock:</h4>"+" "+stock.ticker;
			output +="<h4>Amount:</h4>"+" "
			+"<input id='textAdjAmount' type='text' class='form-control' value='"+response.amount+"'>";
			output +="<h4>Long Average Length:</h4>"+" "
			+"<input id='textAdjLong' type='text' class='form-control' value='"+response.long_average_length+"'>";
			output +="<h4>Short Average Length:</h4>"+" "
			+"<input id='textAdjShort' type='text' class='form-control' value='"+response.short_average_length+"'>";	
			output +="<h4>Exit Condition:</h4>"+" "
			+"<input id='textAdjExit' type='text' class='form-control' value='"+response.exit_condition+"'>";
			output += "<button button id='adjustConfirm' type='button' class='btn btn-success'value='"
			+response.strategy_id+"'>Confirm</button>";
			$("#strategyUpdate").html(output);
			
			$( "#dialogStragAdj").dialog('open');
			updateSetup();
		},
		error: function (err) {
			alert("Error: " + err.responseText);
		}
	});
}

//click handler for adjusting strategy
function updateSetup(){
	$("#adjustConfirm").click(updateStrategy);
	
}

//updates strategy with values from user input
function updateStrategy(){

	var id = $(this).val();
	var strag = getStrategy(id);
	var long = parseFloat($("#textAdjLong").val()); 
	var short = parseFloat($("#textAdjShort").val());
	var exit = parseFloat($("#textAdjExit").val());
	var amount = parseFloat($("#textAdjAmount").val());

	var params = {strategy_id:id, strategy_status: strag.strategy_status, long_average_length:long,
			short_average_length:short,
			  stock_id: strag.stock_id,
			  amount:amount,
			  exit_condition:exit,
			  in_option:strag.in_option
			  };
	
	var paramsJson = JSON.stringify(params);
	//alert(paramsJson);
	$.ajax({
		url: "/strategies",
		data: paramsJson,
		dataType: "json",
		type: "POST",
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			//alert(response);
			
			$( "#dialogStragAdj").dialog('close');
			$( "#dialogStragAdjSuccess").dialog('open');
			strategyList();
		},
		error: function (err) {
			alert("Error: " + err.responseText)
		//	$( "#dialogFailSave").dialog('open');
		}
	});
}
//updates state of strategy to either active or inactive
function updateState(){
	var id = $(this).val();
	//alert(id);
	var strag = getStrategy(id);
	var actState;
	var params;
	if(strag.strategy_status == 1){
		params = {strategy_id:strag.strategy_id, strategy_status: 0, 
				  long_average_length:strag.long_average_length,
				  short_average_length:strag.short_average_length,
				  stock_id: strag.stock_id,
				  amount:strag.amount,
				  exit_condition:strag.exit_condition,
				  in_option: strag.in_option
				  };
		actState = "Strategy Deactivated";
	}
	else{
		params = {strategy_id:strag.strategy_id, strategy_status: 1, 
				  long_average_length:strag.long_average_length,
				  short_average_length:strag.short_average_length,
				  stock_id: strag.stock_id,
				  amount:strag.amount,
				  exit_condition:strag.exit_condition,
				  in_option:strag.in_option
				  };
		actState = "Strategy Activated";
	}
	var paramsJson = JSON.stringify(params);
	//alert(paramsJson);
	$.ajax({
		url: "/strategies",
		data: paramsJson,
		dataType: "json",
		type: "POST",
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			var output="<h4>"+actState+"</h4>";
			$("#actStatus").html(output);
			
			$( "#dialogStragAct").dialog('open');
			strategyList();
		},
		error: function (err) {
			alert("Error: " + err.responseText)
		//	$( "#dialogFailSave").dialog('open');
		}
	});
}

//populates strategy panel with strategies from db
function strategyList(){

	$.ajax({
		url: "/strategies",
		dataType: "json",
		type: "GET",
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			var output="<div>";
			for(var i=0;i<response.length;i++)
				{
				output += "<div class='panel panel-default'>";
				output += "<div class='panel-heading'>";
				if(response[i].strategy_status == 1)
				{
					output += "<h3 class='panel-title'>Active</h3>";
				}
				else{
					output += "<h3 class='panel-title'>Inactive</h3>";
				}
				output += "</div>";
				output += "<div class='panel-body'>";
				var stock = getTicker(response[i].stock_id);
				output += "<h3><strong>"+ stock.ticker+"</strong></h3><br /><strong>STOCK:"
				+" "+response[i].amount+
				"</strong><br /><strong>EXIT:"
				+" "+response[i].exit_condition
				+"</strong><br />";
				output += "</div>";
				output += "<div class='panel-footer'>";
				output += "<div class='btn-group' role='group' aria-label='strategy_options'>";
				
				if(response[i].strategy_status == 1)
				{
					var actId = "buttonAct"+i;
					var adjId = "buttonAdj"+i;
					output += "<button button id='"+adjId+"' type='button' class='btn btn-default' value='"+response[i].strategy_id+"'>Adjust</button>";
					output += "<button button id='"+actId+"' type='button' class='btn btn-danger' value='"+response[i].strategy_id+"'>De-Activate</button>";
				}
				else{
					var actId = "buttonAct"+i;
					var adjId = "buttonAdj"+i;
					output += "<button button id='"+adjId+"' type='button' class='btn btn-default' value='"+response[i].strategy_id+"'>Adjust</button>";
					output += "<button button id='"+actId+"' type='button' class='btn btn-success'value='"+response[i].strategy_id+"'>Activate</button>";
				}
				output += "</div>";
				output += "</div>";
				output += "</div>";
				}
			output += "</div>";
			$("#resultsStrategy").html(output);
			actSetup(response.length);
		},
		error: function (err) {
			alert("Error: " + err.responseText)
		}
	});	
}

