/**
 * 
 */
$(document).ready(startup);

function startup(){
	
	$("#buttonFind").click(find);
	//find dialog for find
	$( "#dialogStockNotFound" ).dialog({
	      modal: true,
	      autoOpen: false,
	      buttons: {
	        Ok: function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
	
	$( "#dialogApplyStrag" ).dialog({
	      modal: true,
	      autoOpen: false,
	      buttons: {
	        Ok: function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
	
	$( "#dialogStragSuccess" ).dialog({
	      modal: true,
	      autoOpen: false,
	      buttons: {
	        Ok: function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
}

function applySetup(){
	$("#buttonApply").click(apply);
	$("#buttonConfirm").click(confirm);
}

function apply(){
	$( "#dialogApplyStrag").dialog('open');
}

function confirm(){
	var stock = find(); 
	var date = new Date();
	var long = parseFloat($("#textLong").val()); 
	var short = parseFloat($("#textShort").val());
	var exit = parseFloat($("#textExit").val());
	var amount = parseFloat($("#textAmount").val());

	var params = {id:-1, strategy_status: true, long_average_length:long,
			short_average_length:short,
			  stock_id: stock.id,
			  amount:amount,
			  exit_condition:exit,
			  in_option: false
			  };
	
	var paramsJson = JSON.stringify(params);
	
	$.ajax({
		url: "/strategies",
		data: paramsJson,
		dataType: "json",
		type: "POST",
		cache: false,
		async: false,
		contentType: 'application/json; charset=utf-8',
		success: function (response) {
			//alert(response);
			
			$( "#dialogApplyStrag").dialog('close');
			strategyList();
			$( "#dialogStragSuccess").dialog('open');
		},
		error: function (err) {
			alert("Error: " + err.responseText)
		//	$( "#dialogFailSave").dialog('open');
		}
	});
	
}

function find(){
	
	var tickerInput = $("#textFind").val();
	
	var url = "/stocks/ticker/"+ tickerInput;
	var stock;
	//alert(url);
		$.ajax({
			url: url,
			type: "GET",
		    dataType: 'json',
			cache: false,
			async: false,
			contentType: 'application/json; charset=utf-8',
			success: function (response) {
				var output="<HR>";
				output += "<h4><b>STOCK:</b> " + response[0].ticker+ "<br/>";
				var last = response.length - 1;				
				var lastArr = response[last];
				output += "<b>PRICE:</b> " + lastArr.price + "</h4>";
				output += "<input id='buttonApply' class='btn btn-default' type='button' value='Apply Strategy'>";
				$("#searchResult").html(output);
				$("#currentStock").html(response[0].ticker);
				$("#currentPrice").html(lastArr.price);
				stock = lastArr;
				applySetup();		
			},
			error: function (err) {
				//alert("Error: " + err.responseText)
				$( "#dialogStockNotFound").dialog('open');
			}
		});
	return stock;
}

