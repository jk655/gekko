package com.citi.training.engine;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.data.bs.StockService;
import com.citi.training.data.bs.StrategyService;
import com.citi.training.data.bs.TradeService;
import com.citi.training.data.bs.UserService;
import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Strategy;
import com.citi.training.data.dto.Trade;
import com.citi.training.data.dto.User;

@Component
public class TradeEngine {

	private static final Logger log = LoggerFactory.getLogger(TradeEngine.class);

	@Autowired
	private TradeService tradeService;

	@Autowired
	private UserService userService;

	@Autowired
	private StockService stockService;

	@Autowired
	private StrategyService strategyService;

	@Autowired
	private StrategyDeterminer determiner;

	@Autowired
	GekkoTradeSender tradeSender;

	@Autowired
	GekkoTradeReceiver tradeReceiver;

	@Scheduled(fixedRate = 10000)
	public void sendTrades() {
		List<Trade> initTrades = tradeService.getTradesByState(Trade.TradeState.INIT);

		log.debug("Processing " + initTrades.size() + " new trades");
		for (Trade trade : initTrades) {
			System.out.println("About to send trade to Active MQ: " + trade.toString());
			tradeService.saveTrade(trade);
			tradeSender.SendTrade(trade);
		}
	}

	@Scheduled(fixedRate = 3000)
	public void getLiveFeed() {
		OnlineFeed onlineFeed = new OnlineFeed();
		stockService.saveStock(onlineFeed.RetrieveStock("GOOG"));
		stockService.saveStock(onlineFeed.RetrieveStock("AAPL"));
		stockService.saveStock(onlineFeed.RetrieveStock("MSFT"));
		stockService.saveStock(onlineFeed.RetrieveStock("NSC"));
		stockService.saveStock(onlineFeed.RetrieveStock("BRK"));
	}

	@Scheduled(fixedRate = 3000)
	public void runStrategies() {
		applyStrategies();
	}

	private void applyStrategies() {
		List<Strategy> strategies = strategyService.getAllStrategies();
		System.out.println("Applying strategies");

		for (Strategy s : strategies) {
			if (s.isStrategy_status() == true) {
				Stock stock = stockService.getStockById(s.getStock_id());
				List<Stock> stocks = stockService.getStocksByTicker(stock.getTicker());

				double previousLongAvg = determiner.getAvg(stocks, s.getLong_average_length(), 1);
				double previousShortAvg = determiner.getAvg(stocks, s.getShort_average_length(), 1);
				double longAvg = determiner.getAvg(stocks, s.getLong_average_length(), 0);
				double shortAvg = determiner.getAvg(stocks, s.getShort_average_length(), 0);

				String result = determiner.determineTrade(shortAvg, longAvg, previousShortAvg, previousLongAvg);
				System.out.println("short: " + shortAvg + "long: " + longAvg + " previosu short" + previousShortAvg
						+ " previous long" + previousLongAvg);
				System.out.println(s.getLong_average_length() + " " + s.getShort_average_length());

				System.out.println(result);

				if (result.equals("BUY")) {

					tradeService.createTrade(stock, Trade.TradeType.BUY.toString(), s.getStrategy_id());
					User user = userService.getUserById(1);
					double total = user.getAmount();
					// System.out.println("total: " + total + " Price being bought: " +
					// stocks.get(stocks.size()).getPrice());
					user.setAmount(total -= stocks.get(stocks.size()).getPrice());
					System.out.println("total: " + total + " after buy");
					userService.saveUser(user);

				} else if (result.equals("SELL")) {
					tradeService.createTrade(stock, Trade.TradeType.SELL.toString(), s.getStrategy_id());
					User user = userService.getUserById(1);
					double total = user.getAmount();
					System.out.println("total: " + total + " Price being sold: " + stock.getPrice());

					user.setAmount(total += stock.getPrice());
					System.out.println("total: " + total + " after sell");

					userService.saveUser(user);
				}
			}
		}
	}

	private void determineBuyOrSell(Trade t) {
		// List<Trade> activeTrades = tradeService.getAllTrades();

		List<Stock> stocks = stockService.getStocksByTicker(t.getStock());
		if (stocks.size() > 0) {

			Stock stock = stocks.get(stocks.size() - 1);
			String tradeResult = determiner.checkCurrentTrades(t, stock.getPrice());

			if (tradeResult.equals("BUY")) {
				List<Stock> latestStocks = stockService.getStocksByTicker(t.getStock());
				Stock latestStock = latestStocks.get(latestStocks.size() - 1);
				buyStock(t, latestStock);
			} else if (tradeResult.equals("SELL")) {
				List<Stock> latestStocks = stockService.getStocksByTicker(t.getStock());
				Stock latestStock = latestStocks.get(latestStocks.size() - 1);
				sellStock(t, latestStock);
			} else {
				System.out.println("Not doing anything with a trade");
			}

		}
	}

	private void sellStock(Trade t, Stock stock) {
		tradeService.createTrade(stock, Trade.TradeType.SELL.toString(), t.getStrategy_id());
		System.out.println("Selling a trade");
	}

	private void buyStock(Trade t, Stock stock) {
		tradeService.createTrade(stock, Trade.TradeType.BUY.toString(), t.getStrategy_id());
		System.out.println("Buying a trade");
	}
}
