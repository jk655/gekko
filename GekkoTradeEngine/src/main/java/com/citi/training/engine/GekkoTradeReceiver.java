package com.citi.training.engine;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.citi.training.data.bs.TradeService;
import com.citi.training.data.dto.Trade;

@Component
public class GekkoTradeReceiver {
	
	private static final Logger log = LoggerFactory.getLogger(TradeEngine.class);
	
	@Autowired
	private TradeService tradeService;
	
	@JmsListener(destination="OrderBroker_Reply")
	public void receive (String xmlReply) {
	
	log.debug("Processing OrderBroker Replies");
	
		if (xmlReply.contains("<result>REJECTED</result>")) {
			Trade tradeReply = new Trade().fromXml(xmlReply);
			tradeReply.stateChange(Trade.TradeState.REJECTED);
			tradeService.saveTrade(tradeReply);
			System.out.println("Received " + tradeReply);
		}
		else if (xmlReply.contains("<result>Partially_Filled</result>")) {
			Trade tradeReply = new Trade().fromXml(xmlReply);
			tradeReply.stateChange(Trade.TradeState.INIT);
			tradeService.saveTrade(tradeReply);
			System.out.println("Resending " + tradeReply);
		}
		else {
			Trade tradeReply = new Trade().fromXml(xmlReply);
			tradeReply.stateChange(Trade.TradeState.FILLED);
			tradeService.saveTrade(tradeReply);
			System.out.println("Received " + tradeReply);
		}
		
		
	}
		
	}

