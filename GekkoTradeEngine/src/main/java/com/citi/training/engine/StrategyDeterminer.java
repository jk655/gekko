package com.citi.training.engine;

import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Trade;

@Component
public class StrategyDeterminer {
		
	public String determineTrade(double shortAvg, double longAvg, double previousShortAvg, double previousLongAvg) {
		if (previousLongAvg > previousShortAvg && longAvg < shortAvg) {
			return "BUY";
		}

		if (previousLongAvg < previousShortAvg && longAvg > shortAvg) {
			return "SELL";
		} else {
			return "No Trade";
		}
	}
	
	public String checkCurrentTrades(Trade trade, double currentPrice) {
		double price = trade.getPrice(); //10
		Trade.TradeType type = trade.getTradeType(); //buy
		double differential = price * 0.001; //1
		
		if(currentPrice >= price+differential || currentPrice <= price-differential) {
			
			if(type.toString() == "BUY") {
				return "SELL";
			}
			else return "BUY";
		}
		
		else return "No Trade";
		
	}

	public double getAvg(List<Stock> stocks, double length, int type) {
		if(length < stocks.size()) {
			length = length + type;
			int first = (int) (stocks.size()-length);
			int second = stocks.size()-type;
			
			List<Stock> longAvgStocks = stocks.subList(first, second);

			double total = 0;
			
			for(Stock s : longAvgStocks) {
				total += s.getPrice();
			}
			
			DecimalFormat df = new DecimalFormat("#.##");
			double avg = total / longAvgStocks.size();
			
			return Double.parseDouble(df.format(avg));
		}
		else return 0;

	}
}
