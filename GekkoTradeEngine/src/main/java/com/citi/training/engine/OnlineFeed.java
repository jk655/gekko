package com.citi.training.engine;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.citi.training.data.dto.Stock;

public class OnlineFeed {
	public String readFeed(String stock){
		
		RestTemplate restTemplate = new RestTemplate();
		
		String url  = "http://feed.conygre.com:8080/MockYahoo/quotes.csv?s="+stock+"&f=s0l1v0";
		ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
		return response.getBody();	
	
	}
	
	public Stock RetrieveStock(String stockName) {
		OnlineFeed onlineFeed = new OnlineFeed();
		String stockFeed = onlineFeed.readFeed(stockName);
		Stock stock = generateStock(stockFeed);
		//System.out.println(stock.toString());
		return stock;
	}

	public Stock generateStock(String feedValue) {
		//System.out.println(feedValue);
		String[] arr = feedValue.split(",");
		
		String ticker = arr[0];
		ticker = ticker.replace("\"", "");
		
		double price = Double.parseDouble(arr[1]);

		String volumeFeed = arr[2];
		String volumeRemoveEndLine = volumeFeed.substring(0, 4);

		int volume = Integer.parseInt(volumeRemoveEndLine);

		return new Stock(-1, ticker, price, volume);
	}
	
}
