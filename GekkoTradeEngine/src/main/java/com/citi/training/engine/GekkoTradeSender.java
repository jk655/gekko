package com.citi.training.engine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;


import com.citi.training.data.dto.Trade;

@Component
public class GekkoTradeSender {
		
	
    @Autowired
	private JmsTemplate jmstemplate;

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(GekkoTradeSender.class,args );
		}
	
	
	public void SendTrade(Trade tradeToSend) {
		jmstemplate.convertAndSend("OrderBroker", Trade.toXml(tradeToSend), message -> {
			message.setStringProperty("Operation", "update");
			message.setJMSCorrelationID(String.valueOf(tradeToSend.getId()));
			return message;
		});
	}
	
}


