package com.citi.training;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Trade;
import com.citi.training.engine.StrategyDeterminer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeEngineTests {

	public StrategyDeterminer s = new StrategyDeterminer();
	List<Stock> stocks;
	List<Stock> stocksSell;
	List<Stock> stocksBuy;
	@Before
	public void createStrategy() {
		stocks =  new ArrayList<Stock>();
		stocks.add(new Stock(1, "GOOG", 100, 1000));
		stocks.add(new Stock(1, "GOOG", 130, 1000));
		stocks.add(new Stock(1, "GOOG", 110, 1000));
		stocks.add(new Stock(1, "GOOG", 150, 1000));
		stocks.add(new Stock(1, "GOOG", 100, 1000));
		stocks.add(new Stock(1, "GOOG", 140, 1000));
		stocks.add(new Stock(1, "GOOG", 120, 1000));
		stocks.add(new Stock(1, "GOOG", 160, 1000));
		stocks.add(new Stock(1, "GOOG", 180, 1000));
		stocks.add(new Stock(1, "GOOG", 200, 1000));
		
		stocksSell =  new ArrayList<Stock>();
		stocksSell.add(new Stock(1, "GOOG", 100, 1000));
		stocksSell.add(new Stock(1, "GOOG", 10, 1000));
		stocksSell.add(new Stock(1, "GOOG", 150, 1000));
		stocksSell.add(new Stock(1, "GOOG", 160, 1000));
		stocksSell.add(new Stock(1, "GOOG", 140, 1000));
		stocksSell.add(new Stock(1, "GOOG", 130, 1000));
		stocksSell.add(new Stock(1, "GOOG", 150, 1000));
		stocksSell.add(new Stock(1, "GOOG", 100, 1000));
		stocksSell.add(new Stock(1, "GOOG", 110, 1000));
		stocksSell.add(new Stock(1, "GOOG", 60, 1000));
		
		stocksBuy =  new ArrayList<Stock>();
		stocksBuy.add(new Stock(1, "GOOG", 100, 1000));
		stocksBuy.add(new Stock(1, "GOOG", 200, 1000));
		stocksBuy.add(new Stock(1, "GOOG", 110, 1000));
		stocksBuy.add(new Stock(1, "GOOG", 140, 1000));
		stocksBuy.add(new Stock(1, "GOOG", 130, 1000));
		stocksBuy.add(new Stock(1, "GOOG", 140, 1000));
		stocksBuy.add(new Stock(1, "GOOG", 10, 1000));
		stocksBuy.add(new Stock(1, "GOOG", 160, 1000));
		stocksBuy.add(new Stock(1, "GOOG", 180, 1000));
		stocksBuy.add(new Stock(1, "GOOG", 120, 1000));
	}

	@Test
	public void testRunStrategies() {

	}

//	@Test
//	public void testGetAvg() {
//		double previousShort = s.getAvg(stocks, 3, 1);
//		double previousLong = s.getAvg(stocks, 8, 1);
//
//		double currentShort = s.getAvg(stocks, 3, 0);
//		double currentLong = s.getAvg(stocks, 8, 0);
//
//		System.out.println(previousShort);
//		System.out.println(currentShort);
//		System.out.println(previousLong);
//		System.out.println(currentLong);
//
//		assertTrue(previousShort == 153.33);
//		assertTrue(currentShort == 180);
//		assertTrue(previousLong == 136.25);
//		assertTrue(currentLong == 145);
//	}

//	@Test
//	public void testDetermineTradeNoTrade() {
//		
//		String result = s.determineTrade(0, 0, 0, 0);
//		System.out.println(result);
//		assertTrue(result.equals("No Trade"));
//	}
//	
//	@Test
//	public void testDetermineTradeBuy() {
//		double previousShort = s.getAvg(stocksBuy, 3, 1);
//		double previousLong = s.getAvg(stocksBuy, 8, 1);
//
//		double currentShort = s.getAvg(stocksBuy, 3, 0);
//		double currentLong = s.getAvg(stocksBuy, 8, 0);
//		
//		System.out.println("ps"+previousShort);
//		System.out.println("cs"+currentShort);
//		System.out.println("pl"+previousLong);
//		System.out.println("cl"+currentLong);
//		
//		String result = s.determineTrade(currentShort, currentLong, previousShort, previousLong);
//		System.out.println(result);
//		assertTrue(result.equals("BUY"));
//	}
	
	@Test
	public void testCheckCurrentTrades() {
		String result = s.checkCurrentTrades(new Trade("goog",10,1000,"BUY", 0), 11);
		System.out.println(result);
		assertTrue(result.equals("SELL"));
	}
	
	@Test
	public void testBadCheckCurrentTrades() {
		String result = s.checkCurrentTrades(new Trade("goog",10,1000,"BUY", 0), 10.00001);
		System.out.println(result);
		assertTrue(result.equals("No Trade"));
	}
	
	@Test
	public void testBuyCheckCurrentTrades() {
		String result = s.checkCurrentTrades(new Trade("goog", 10, 1000,"SELL", 0), 11);
		System.out.println(result);
		assertTrue(result.equals("BUY"));
	}
	
	
	@Test
	public void testDetermineTradeSell() {
		double previousShort = s.getAvg(stocksSell, 3, 1);
		double previousLong = s.getAvg(stocksSell, 8, 1);

		double currentShort = s.getAvg(stocksSell, 3, 0);
		double currentLong = s.getAvg(stocksSell, 8, 0);
		
		System.out.println("ps"+previousShort);
		System.out.println("cs"+currentShort);
		System.out.println("pl"+previousLong);
		System.out.println("cl"+currentLong);
		
		String result = s.determineTrade(currentShort, currentLong, previousShort, previousLong);
		System.out.println(result);
		assertTrue(result.equals("SELL"));
	}
}
