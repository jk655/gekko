package com.citi.training.data.dao;

import java.util.List;

import com.citi.training.data.dto.Trade;

public interface TradeRepository {

    public Trade saveTrade(Trade Trade);

    public Trade getTradeById(int Id);

    public List<Trade> getTradesByStock(String stock);
    
    public List<Trade> getTradesByStrategyId(int id);

    public List<Trade> getTradesByState(Trade.TradeState state);

    public List<Trade> getAllTrades();

}
