package com.citi.training.data.dto;

public class Stock {
	
	private int id = -1;
	private String ticker;
	private double price;
	private int volume;
	
	public Stock() {}
	
	public Stock(int id, String ticker, double price, int volume) {
		this.id = id;
		this.price = price;
		this.volume = volume;
		this.ticker = ticker;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	@Override
	public String toString() {
		return "Stock [id=" + id + ", ticker=" + ticker + ", price=" + price + ", volume=" + volume + "]";
	}
	
	
}