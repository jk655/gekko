package com.citi.training.data.bs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.training.data.dao.JdbcStockRepository;
import com.citi.training.data.dao.JdbcStrategyRepository;
import com.citi.training.data.dao.JdbcTradeRepository;
import com.citi.training.data.dao.JdbcUserRepository;
import com.citi.training.data.dao.TradeRepository;
import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Strategy;
import com.citi.training.data.dto.Trade;
import com.citi.training.data.dto.User;

@Service
public class UserService {
	@Autowired
    private JdbcUserRepository userRepository;

    public User saveUser(User user) {
        return userRepository.saveUser(user);
    }

    public User getUserById(int id) {
        return userRepository.getUserById(id);
    }

    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }
    
    public double getAmountBuy() {
    	return userRepository.getTotalBuy();
    }
    
    public double getAmountSell() {
    	return userRepository.getTotalBuy();
    }
    
    

}
