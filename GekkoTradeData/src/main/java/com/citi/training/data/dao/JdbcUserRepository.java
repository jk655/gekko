package com.citi.training.data.dao;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Strategy;
import com.citi.training.data.dto.Trade;
import com.citi.training.data.dto.User;
import com.mysql.jdbc.Statement;

@Repository("JdbcUserRepository")
public class JdbcUserRepository {

    private static final Logger log = LoggerFactory.getLogger(JdbcUserRepository.class);

    @Value("${user.table.name:users}")
    private String tableName = "users";

    private final String insertSQL = "INSERT INTO " + tableName + " (username, amount) values "
    		+ "(:username, :amount)";
    
//    INSERT INTO strategies values(1,true,30,7,1,10,10.5);

    private final String updateSQL = "UPDATE " + tableName + " SET username=:username, " +
                                     "amount=:amount" +
                                    
                                     " WHERE user_id=:user_id";

    private final String selectByUserIdSQL = "SELECT * FROM " + tableName + " WHERE user_id=?";
    private final String selectAllSQL = "SELECT * FROM " + tableName;
    private final String selectBuyTotal =  "SELECT SUM(price) FROM trades WHERE tradeType = \"BUY\" && state = \"FILLED\";";  
    private final String selectSellTotal = "SELECT SUM(price) FROM trades WHERE tradeType = \"SELL\" && state = \"FILLED\";";

    
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    // returns the object given
    // if id < -1 then it will be inserted, otherwise updated
    public User saveUser(User user) {
        BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(user);
        // use string value of tradeType and state enums
        
        namedParameters.registerSqlType("username", Types.VARCHAR);
        namedParameters.registerSqlType("amount", Types.DOUBLE);

        if (user.getUser_id() < 0) {

            log.debug("Inserting user: " + user.toString());

            KeyHolder keyHolder = new GeneratedKeyHolder();
            namedParameterJdbcTemplate.update(insertSQL, namedParameters, keyHolder);
            user.setUser_id(keyHolder.getKey().intValue());
            
        } else {
            log.debug("Updating user: " + user.toString());
            namedParameterJdbcTemplate.update(updateSQL, namedParameters);
        }
        return user;
    }

    public User getUserById(int id) {
        log.debug("JdbcTradeRepo getById: " + id);
        List<User> users = jdbcTemplate.query(selectByUserIdSQL,
                                                new BeanPropertyRowMapper<User>(User.class),
                                                id);

        return users.get(0);
    }
    
    public double getTotalBuy() {
        Double total = jdbcTemplate.queryForObject((selectBuyTotal),  Double.class);
        return total;
    }
    
    public double getTotalSell() {
        Double total = jdbcTemplate.queryForObject((selectSellTotal),  Double.class);
        return total;
    }
    
    public List<User> getAllUsers() {
        log.debug("JdbcTradeRepo getAll");
        List<User> users = jdbcTemplate.query(selectAllSQL,
                                                new BeanPropertyRowMapper<User>(User.class));

        return users;
    }
}