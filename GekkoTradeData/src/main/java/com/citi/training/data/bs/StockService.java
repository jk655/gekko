package com.citi.training.data.bs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.training.data.dao.JdbcStockRepository;
import com.citi.training.data.dao.JdbcTradeRepository;
import com.citi.training.data.dao.TradeRepository;
import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Trade;

@Service
public class StockService {
	@Autowired
    private JdbcStockRepository stockRepository;

    public Stock saveStock(Stock stock) {
        return stockRepository.saveStock(stock);
    }

    public Stock getStockById(int id) {
        return stockRepository.getStockById(id);
    }

    public List<Stock> getStocksByTicker(String ticker) {
        return stockRepository.getStocksByTicker(ticker);
    }

    public List<Stock> getAllStocks() {
        return stockRepository.getAllStocks();
    }

}
