package com.citi.training.data.dto;

public class Strategy {
	
	private int strategy_id = -1;
	private boolean strategy_status;
	private double long_average_length;
	private double short_average_length;
	private int stock_id;
	private boolean in_option = false;
	
	public boolean isIn_option() {
		return in_option;
	}

	public void setIn_option(boolean in_option) {
		this.in_option = in_option;
	}

	public int getStock_id() {
		return stock_id;
	}

	public void setStock_id(int stock_id) {
		this.stock_id = stock_id;
	}

	private int amount;
	private double exit_condition;
	
	public Strategy() {}
	
	public Strategy(int strategy_id, boolean strategy_status, double long_average_length, double short_average_length, int trade_id,
			int amount, double exit_condition) {
		this.strategy_id = strategy_id;
		this.strategy_status = strategy_status;
		this.long_average_length = long_average_length;
		this.short_average_length = short_average_length;
		this.stock_id = trade_id;
		this.amount = amount;
		this.exit_condition = exit_condition;
	}

	public double getLong_average_length() {
		return long_average_length;
	}

	public void setLong_average_length(double long_average_length) {
		this.long_average_length = long_average_length;
	}

	public double getShort_average_length() {
		return short_average_length;
	}

	public void setShort_average_length(double short_average_length) {
		this.short_average_length = short_average_length;
	}

	public int getStrategy_id() {
		return strategy_id;
	}
	public void setStrategy_id(int strategy_id) {
		this.strategy_id = strategy_id;
	}
	public boolean isStrategy_status() {
		return strategy_status;
	}
	public void setStrategy_status(boolean strategy_status) {
		this.strategy_status = strategy_status;
	}

	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public double getExit_condition() {
		return exit_condition;
	}
	public void setExit_condition(double exit_condition) {
		this.exit_condition = exit_condition;
	}
	
	@Override
	public String toString() {
		return "Strategy [strategy_id=" + strategy_id + ", strategy_status=" + strategy_status + ", long_average="
				+ long_average_length + ", short_average=" + short_average_length + ", trade_id=" + stock_id + ", amount=" + amount
				+ ", exit_condition=" + exit_condition + "]";
	}
}