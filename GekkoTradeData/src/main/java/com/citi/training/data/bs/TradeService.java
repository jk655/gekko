package com.citi.training.data.bs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.training.data.dao.TradeRepository;
import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Trade;

@Service
public class TradeService {

    @Autowired
    private TradeRepository tradeRepository;

    public Trade saveTrade(Trade newTrade) {
        return tradeRepository.saveTrade(newTrade);
    }

    public Trade createTrade(Stock stock, String tradeType, int strategy_id) {
        return tradeRepository.saveTrade(new Trade(stock.getTicker(), stock.getPrice(), stock.getVolume(), tradeType, strategy_id));
    }

    public Trade getTradeById(int id) {
        return tradeRepository.getTradeById(id);
    }

    public List<Trade> getTradesByStock(String stock) {
        return tradeRepository.getTradesByStock(stock);
    }

    public List<Trade> getTradesByState(Trade.TradeState state) {
        return tradeRepository.getTradesByState(state);
    }
    
    public List<Trade> getTradesByStrategyId(int id) {
        return tradeRepository.getTradesByStrategyId(id);
    }

    public List<Trade> getAllTrades() {
        return tradeRepository.getAllTrades();
    }
}