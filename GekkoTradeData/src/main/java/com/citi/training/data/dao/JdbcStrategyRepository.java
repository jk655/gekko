package com.citi.training.data.dao;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Strategy;
import com.citi.training.data.dto.Trade;
import com.mysql.jdbc.Statement;

@Repository("JdbcStrategyRepository")
public class JdbcStrategyRepository {

    private static final Logger log = LoggerFactory.getLogger(JdbcStrategyRepository.class);

    @Value("${strategy.table.name:strategies}")
    private String tableName = "strategies";

    private final String insertSQL = "INSERT INTO " + tableName + " ( strategy_status, long_average_length, short_average_length,stock_id, amount, exit_condition, in_option) values "
    		+ "(:strategy_status, :long_average_length, :short_average_length, :stock_id, :amount, :exit_condition, :in_option)";
    
    private final String updateSQL = "UPDATE " + tableName + " SET strategy_status=:strategy_status, " +
                                     "long_average_length=:long_average_length, short_average_length=:short_average_length," +
                                     
                                     "stock_id=:stock_id, amount=:amount" +
                                     
                                     ",exit_condition=:exit_condition, in_option=:in_option" +
                                     " WHERE strategy_id=:strategy_id";

    private final String selectByStrategyIdSQL = "SELECT * FROM " + tableName + " WHERE strategy_id=?";
    private final String selectByStockIdSQL = "SELECT * FROM " + tableName + " WHERE stock_id=?";
    private final String selectAllSQL = "SELECT * FROM " + tableName;
    private final String selectCount = "select count(*) from " + tableName;
    
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    // returns the object given
    // if id < -1 then it will be inserted, otherwise updated
    public Strategy saveStrategy(Strategy strategy) {
    	System.out.println(strategy.toString());
        BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(strategy);
        // use string value of tradeType and state enums
        
        namedParameters.registerSqlType("strategy_status", Types.BOOLEAN);
        namedParameters.registerSqlType("long_average_length", Types.DOUBLE);
        namedParameters.registerSqlType("short_average_length", Types.DOUBLE);
        namedParameters.registerSqlType("stock_id", Types.INTEGER);
        namedParameters.registerSqlType("amount", Types.INTEGER);
        namedParameters.registerSqlType("exit_condition", Types.DOUBLE);
        namedParameters.registerSqlType("in_option", Types.BOOLEAN);
        if (strategy.getStrategy_id() < 0) {

            log.debug("Inserting trade: " + strategy);

            KeyHolder keyHolder = new GeneratedKeyHolder();
            namedParameterJdbcTemplate.update(insertSQL, namedParameters, keyHolder);
            strategy.setStrategy_id(keyHolder.getKey().intValue());
            System.out.println(strategy.toString());

            
        } else {
            log.debug("Updating strategy: " + strategy);
            namedParameterJdbcTemplate.update(updateSQL, namedParameters);
        }
        log.info("JdbcRepo returning strategy: " + strategy);
        return strategy;
    }

    public Strategy getStrategyByStrategyId(int id) {
        log.debug("JdbcTradeRepo getById: " + id);
        List<Strategy> strategies = jdbcTemplate.query(selectByStrategyIdSQL,
                                                new BeanPropertyRowMapper<Strategy>(Strategy.class),
                                                id);

        log.debug("Query for id <" + id + "> returned list: " + strategies);
        return strategies.get(0);
    }
    

    public Strategy getStrategyByStockId(int id) {
        log.debug("JdbcTradeRepo getById: " + id);
        List<Strategy> strategies = jdbcTemplate.query(selectByStockIdSQL,
                                                new BeanPropertyRowMapper<Strategy>(Strategy.class),
                                                id);

        log.debug("Query for id <" + id + "> returned list: " + strategies);
        return strategies.get(0);
    }

    public List<Strategy> getAllStrategies() {
        log.debug("JdbcTradeRepo getAll");
        List<Strategy> strategies = jdbcTemplate.query(selectAllSQL,
                                                new BeanPropertyRowMapper<Strategy>(Strategy.class));

        log.debug("Query for all returned list of size: " + strategies.size());
        return strategies;
    }
}