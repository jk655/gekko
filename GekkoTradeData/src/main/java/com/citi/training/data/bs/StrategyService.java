package com.citi.training.data.bs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.training.data.dao.JdbcStockRepository;
import com.citi.training.data.dao.JdbcStrategyRepository;
import com.citi.training.data.dao.JdbcTradeRepository;
import com.citi.training.data.dao.TradeRepository;
import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Strategy;
import com.citi.training.data.dto.Trade;

@Service
public class StrategyService {
	@Autowired
    private JdbcStrategyRepository strategyRepository;

    public Strategy saveStrategy(Strategy strategy) {
        return strategyRepository.saveStrategy(strategy);
    }

    public Strategy getStrategyByStrategyId(int id) {
        return strategyRepository.getStrategyByStrategyId(id);
    }

    public Strategy getStrategyByStockId(int id) {
        return strategyRepository.getStrategyByStockId(id);
    }

    public List<Strategy> getAllStrategies() {
        return strategyRepository.getAllStrategies();
    }

}
