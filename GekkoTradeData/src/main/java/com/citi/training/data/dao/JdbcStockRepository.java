package com.citi.training.data.dao;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Trade;

@Repository("JdbcStockRepository")
public class JdbcStockRepository {

    private static final Logger log = LoggerFactory.getLogger(JdbcStockRepository.class);

    @Value("${stock.table.name:stocks}")
    private String tableName = "stocks";

    private final String insertSQL = "INSERT INTO " + tableName + " (ticker, price, " +
                                     "volume) values (:ticker, :price, " +
                                     ":volume)";
    
//    INSERT INTO stocks
//    VALUES (0, "GOOG", 20.20, 1000);

    private final String updateSQL = "UPDATE " + tableName + " SET ticker=:ticker, " +
                                     "price=:price, volume=:volume" +
                                     " WHERE id=:id";

    private final String selectByIdSQL = "SELECT * FROM " + tableName + " WHERE id=?";
    private final String selectByTickerSQL = "SELECT * FROM " + tableName + " WHERE ticker=?";
    private final String selectAllSQL = "SELECT * FROM " + tableName;
    
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    // returns the object given
    // if id < -1 then it will be inserted, otherwise updated
    public Stock saveStock(Stock stock) {
        BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(stock);
        // use string value of tradeType and state enums
        namedParameters.registerSqlType("ticker", Types.VARCHAR);
        namedParameters.registerSqlType("price", Types.DOUBLE);
        namedParameters.registerSqlType("volume", Types.INTEGER);


        if (stock.getId() < 0) {
            // insert
            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(insertSQL,namedParameters, keyHolder);
            stock.setId(keyHolder.getKey().intValue());
        } else {
            namedParameterJdbcTemplate.update(updateSQL, namedParameters);
        }
        return stock;
    }

    public Stock getStockById(int id) {
        List<Stock> stocks = jdbcTemplate.query(selectByIdSQL,
                                                new BeanPropertyRowMapper<Stock>(Stock.class),
                                                id);

        return stocks.get(0);
    }

  

    public List<Stock> getStocksByTicker(String ticker) {
        List<Stock> stocks = jdbcTemplate.query(selectByTickerSQL,
                                                new BeanPropertyRowMapper<Stock>(Stock.class),
                                                ticker.toString());

        return stocks;
    }

    public List<Stock> getAllStocks() {
        List<Stock> stocks = jdbcTemplate.query(selectAllSQL,
                                                new BeanPropertyRowMapper<Stock>(Stock.class));

        return stocks;
    }
}