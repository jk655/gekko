package com.citi.training.data.entities;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Trade;

public class StockTest {

	@Test
    public void test_createSetGetStock_succeeds() {
		int testId = 1;
		String testTicker = "GOOG";
		double testPrice = 234.45;
		int testVolume = 1000;

        Stock stock = new Stock();

        stock.setId(testId);
        stock.setTicker(testTicker);
        stock.setPrice(testPrice);
        stock.setVolume(testVolume);

        assertTrue(stock.getId() == testId);
        assertTrue(stock.getTicker().equals(testTicker));
        assertTrue(stock.getPrice() == testPrice);
        assertTrue(stock.getVolume() == testVolume);
    }
	
	@Test
    public void test_createStock_succeeds() {
		int testId = 1;
		String testTicker = "GOOG";
		double testPrice = 234.45;
		int testVolume = 1000;
        Stock stock = new Stock(testId, testTicker, testPrice, testVolume);

        assertTrue(stock.getId() == testId);
        assertTrue(stock.getTicker().equals(testTicker));
        assertTrue(stock.getPrice() == testPrice);
        assertTrue(stock.getVolume() == testVolume);
    }

}
