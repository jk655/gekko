package com.citi.training.data.repository;

import static org.junit.Assert.*;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.data.TestApplication;
import com.citi.training.data.dao.JdbcStockRepository;
import com.citi.training.data.dto.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=TestApplication.class)
public class JdbcStockRepositoryTest {
	
	private static final Logger log = LoggerFactory.getLogger(JdbcStockRepositoryTest.class);

	@Resource(name="JdbcStockRepository")
    private JdbcStockRepository stockRepository;

    private Stock testStock;

    @Before
    public void createStock() {
        int testId = -1;
		String testTicker = "GOOG";
		double testPrice = 234.45;
		int testVolume = 1000;
		this.testStock = new Stock(testId, testTicker, testPrice, testVolume);
    }
	
    @Test
    public void test_savingAndRetrieving() {
        int orig_id = testStock.getId();
        Stock newStock = stockRepository.saveStock(testStock);
        assertFalse(orig_id == newStock.getId());
        assertTrue(newStock == testStock);

        Stock retStock = stockRepository.getStockById(newStock.getId());
        log.debug("GetById returned: " + retStock);
        log.debug("newStock returns: " + newStock);
        assertTrue(retStock.getId() == newStock.getId());
        assertTrue(retStock.getTicker().equals(newStock.getTicker()));
        assertTrue(retStock.getPrice() == newStock.getPrice());
        assertTrue(retStock.getVolume() == newStock.getVolume());
        
        // add another trade
        testStock.setId(-1);
        retStock = stockRepository.saveStock(testStock);
        
        List<Stock> stocks = stockRepository.getAllStocks();
        assertTrue(stocks.size() >= 2);
        
        stocks = stockRepository.getStocksByTicker(testStock.getTicker());
        assertTrue(stocks.size() >= 2);
        
        double testPrice = 310.20;
        stocks.get(0).setPrice(testPrice);
        stockRepository.saveStock(stocks.get(0));
        retStock = stockRepository.getStockById(stocks.get(0).getId());
        assertTrue(retStock.getPrice() == testPrice);

        int testVolume = 800;
        stocks.get(0).setVolume(testVolume);
        stockRepository.saveStock(stocks.get(0));
        retStock = stockRepository.getStockById(stocks.get(0).getId());
        assertTrue(retStock.getVolume() == testVolume);
    }

}
