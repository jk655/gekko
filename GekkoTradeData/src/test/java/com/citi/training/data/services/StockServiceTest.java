package com.citi.training.data.services;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.data.TestApplication;
import com.citi.training.data.bs.StockService;
import com.citi.training.data.bs.TradeService;
import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestApplication.class)
public class StockServiceTest {
	
	@Autowired
    private StockService stockService;

	private Stock testStock; 

    @Before
    public void createStock() {
		testStock = new Stock(1, "GOOG", 234.45, 1000);
    }
    
    @Test
	public void stockService_SaveStock_ReturnStock() {
		Stock returnedStock = stockService.saveStock(testStock);
		assertTrue(returnedStock.getId() == testStock.getId());
	}
    
    @Test
	public void stockService_getStockById_ReturnStock() {
		Stock returnedStock = stockService.getStockById(testStock.getId());
		assertTrue(returnedStock.getPrice() == testStock.getPrice());
	}
    
    @Test
   	public void stockService_getStockByTicker_ReturnStockList() {
    	List<Stock> returnedStockList = stockService.getStocksByTicker(testStock.getTicker());
   		assertTrue(returnedStockList.size() > 0);
   	}
    
    @Test
   	public void stockService_getAllStocks_ReturnStockList() {
    	List<Stock> returnedStockList = stockService.getAllStocks();
   		assertTrue(returnedStockList.size() > 0);
   	}

}
