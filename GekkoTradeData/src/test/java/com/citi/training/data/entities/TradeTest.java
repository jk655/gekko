package com.citi.training.data.entities;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.citi.training.data.dto.Trade;

public class TradeTest {

    @Test
    public void test_createSetGetTrade_succeeds() {
        int testTradeId = 99;
        String testTradeStock = "GOOG";
        Trade.TradeType testTradeType = Trade.TradeType.SELL;

        Trade trade = new Trade();

        trade.setId(testTradeId);
        trade.setStock(testTradeStock);
        trade.setTradeType(testTradeType);

        assertTrue(trade.getId() == testTradeId);
        assertTrue(trade.getStock().equals(testTradeStock));
        assertTrue(trade.getTradeType() == testTradeType);
    }

    @Test
    public void test_createTrade_succeeds() {
        String testStock = "AMZN";
        Trade trade = new Trade(testStock, 0, 0, Trade.TradeType.BUY, 0);

        assertTrue(trade.getStock().equals(testStock));
        assertTrue(trade.toString() != null);
        assertTrue(trade.toString().contains(testStock));
    }

    @Test
    public void test_tradeEquals() {
        Trade trade1 = new Trade("MSFT", 0, 0, Trade.TradeType.BUY, 0);
        Trade trade2 = new Trade("MSFT", 0, 0, Trade.TradeType.BUY, 0);
        Trade trade3 = new Trade("MSFT", 0, 0, Trade.TradeType.SELL, 0);
        Trade trade4 = new Trade("AMZN", 0, 0, Trade.TradeType.SELL, 0);

        // Give trade1 and trade2 the same lastStateChange time so they're equal
        trade2.setLastStateChange(trade1.getLastStateChange());

        assertFalse(trade1.equals(null));
        assertFalse(trade1.equals(new Integer(0)));
        assertFalse(trade1.equals(trade3));
        assertTrue(trade1.equals(trade2));
        assertFalse(trade1.equals(trade4));

        // pause so lastStateChange will be different
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {}

        trade2.stateChange(Trade.TradeState.FILLED);
        assertFalse(trade1.equals(trade2));

        assertFalse(trade1.equals(trade2));
    }
}
