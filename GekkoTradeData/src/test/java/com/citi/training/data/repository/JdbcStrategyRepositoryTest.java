package com.citi.training.data.repository;

import static org.junit.Assert.*;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.data.TestApplication;
import com.citi.training.data.dao.JdbcStockRepository;
import com.citi.training.data.dao.JdbcStrategyRepository;
import com.citi.training.data.dto.Stock;
import com.citi.training.data.dto.Strategy;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=TestApplication.class)
public class JdbcStrategyRepositoryTest {
	
	private static final Logger log = LoggerFactory.getLogger(JdbcStrategyRepositoryTest.class);

	@Resource(name="JdbcStrategyRepository")
    private JdbcStrategyRepository strategyRepository;
	
    private Strategy testStrategy;

    @Before
    public void createStrategy() {
    	int strategy_id = -1;
    	boolean strategy_status = true;
    	double long_average_length = 30;
    	double short_average_length = 7;
    	int trade_id = 1;
    	int amount = 1000;
    	double exit_condition = 10.5;
		this.testStrategy = new Strategy(strategy_id, strategy_status, long_average_length, 
				short_average_length, trade_id, amount, exit_condition);
    }
    
    @Test
    public void test_savingAndRetrieving() {
        int orig_id = testStrategy.getStrategy_id();
        Strategy newStrategy = strategyRepository.saveStrategy(testStrategy);
        System.out.println("Test Strategy " + testStrategy.toString());
        System.out.println("New Strategy " + newStrategy.toString());
        assertFalse(orig_id == newStrategy.getStrategy_id());
        assertTrue(newStrategy == testStrategy);

//        Stock retStock = stockRepository.getStockById(newStock.getId());
//        log.debug("GetById returned: " + retStock);
//        log.debug("newStock returns: " + newStock);
//        assertTrue(retStock.getId() == newStock.getId());
//        assertTrue(retStock.getTicker().equals(newStock.getTicker()));
//        assertTrue(retStock.getPrice() == newStock.getPrice());
//        assertTrue(retStock.getVolume() == newStock.getVolume());
//        
//        // add another trade
//        testStock.setId(-1);
//        retStock = stockRepository.saveStock(testStock);
//        
//        List<Stock> stocks = stockRepository.getAllStocks();
//        assertTrue(stocks.size() >= 2);
//        
//        stocks = stockRepository.getStocksByTicker(testStock.getTicker());
//        assertTrue(stocks.size() >= 2);
//        
//        double testPrice = 310.20;
//        stocks.get(0).setPrice(testPrice);
//        stockRepository.saveStock(stocks.get(0));
//        retStock = stockRepository.getStockById(stocks.get(0).getId());
//        assertTrue(retStock.getPrice() == testPrice);
//
//        int testVolume = 800;
//        stocks.get(0).setVolume(testVolume);
//        stockRepository.saveStock(stocks.get(0));
//        retStock = stockRepository.getStockById(stocks.get(0).getId());
//        assertTrue(retStock.getVolume() == testVolume);
    }

}
